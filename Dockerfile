FROM    node:12.10.0-alpine

ARG     SWAGGER_COMBINE_VERSION

RUN     npm install -g swagger-combine@${SWAGGER_COMBINE_VERSION}
