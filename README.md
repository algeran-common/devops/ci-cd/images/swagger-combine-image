SWAGGER COMBINE IMAGE
---------------------

Image that can combine multiple swagger specifications in one.
Image contains node.js with installed modules:

1. Added [swagger-combine module](https://www.npmjs.com/package/swagger-combine)
